package com.elevenware.hmrc.cart;

public interface ShoppingCart {
    void scan(Product product);
    void visit(ShoppingCartVisitor visitor);
}
