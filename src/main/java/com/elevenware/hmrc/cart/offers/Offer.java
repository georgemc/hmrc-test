package com.elevenware.hmrc.cart.offers;

import com.elevenware.hmrc.cart.Product;

import java.math.BigDecimal;

public interface Offer {
    BigDecimal applyTo(Product product);
}
