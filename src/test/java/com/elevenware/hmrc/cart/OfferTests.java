package com.elevenware.hmrc.cart;

import com.elevenware.hmrc.cart.offers.ThreeForTwoOffer;
import com.elevenware.hmrc.cart.offers.TwoForOneOffer;
import org.junit.Test;

public class OfferTests extends AbstractShoppingCartTests {

    @Test
    public void applesOfferIsNotAppliedToSingleApple() {

        cart.scan(APPLE);

        assertReceipt("0.60");

    }

    @Test
    public void applesOfferIsAppliedToTwoApples() {
        
        cart.scan(APPLE);
        cart.scan(APPLE);

        assertReceipt("0.60");

    }
    
    @Test
    public void applesOfferIsAppliedOnceForThreeApples() {

        cart.scan(APPLE);
        cart.scan(APPLE);
        cart.scan(APPLE);

        assertReceipt("1.20");
        
    }

    @Test
    public void applesOfferIsAppliedTwiceForFourApples() {

        cart.scan(APPLE);
        cart.scan(APPLE);
        cart.scan(APPLE);
        cart.scan(APPLE);

        assertReceipt("1.20");

    }
    
    @Test
    public void applesOfferIsNotAppliedToOranges() {

        cart.scan(APPLE);
        cart.scan(APPLE);
        cart.scan(APPLE);
        cart.scan(APPLE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);

        assertReceipt("1.70");
        
    }
    
    @Test
    public void orangesOfferAppliedToThreeOranges() {

        cart.scan(ORANGE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);
        
        assertReceipt("0.50");
    }

    @Test
    public void orangesOfferNotAppliedToAllFiveOranges() {

        cart.scan(ORANGE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);

        assertReceipt("1.00");
    }

    @Test
    public void orangesOfferAppliedToCorrectNumberOfOranges() {

        cart.scan(ORANGE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);

        assertReceipt("1.50");
    }
    
    @Override
    protected void addOffersToTill(Till till) {
        till.addOffer(new TwoForOneOffer(APPLE));
        till.addOffer(new ThreeForTwoOffer(ORANGE));
    }
}
