package com.elevenware.hmrc.cart;

public interface ShoppingCartVisitor {
    
    void visit(Product p);
    
}
