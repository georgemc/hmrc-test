package com.elevenware.hmrc.cart.offers;

import com.elevenware.hmrc.cart.Product;
import com.elevenware.hmrc.cart.offers.Offer;

import java.math.BigDecimal;

public class NoOffers implements Offer {
    @Override
    public BigDecimal applyTo(Product product) {
        return product.getPrice();
    }
}
