package com.elevenware.hmrc.cart.offers;

import com.elevenware.hmrc.cart.Product;
import com.elevenware.hmrc.cart.offers.Offer;

import java.math.BigDecimal;

public class TwoForOneOffer implements Offer {
    
    private static final BigDecimal ZERO = new BigDecimal("0.00");
    private boolean nextOneFree = true;
    private final Product product;

    public TwoForOneOffer(Product product) {
        this.product = product;
    }

    @Override
    public BigDecimal applyTo(Product product) {
        if(!this.product.equals(product)) {
            return ZERO;
        }
        nextOneFree = !nextOneFree;
        return nextOneFree ? ZERO : product.getPrice();
    }
}
