package com.elevenware.hmrc.cart;

import com.elevenware.hmrc.cart.offers.Offer;

public interface Till {
    
    Receipt checkout(ShoppingCart cart);

    void addOffer(Offer offer);
}
