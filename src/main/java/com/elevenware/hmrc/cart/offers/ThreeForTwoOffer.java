package com.elevenware.hmrc.cart.offers;

import com.elevenware.hmrc.cart.Product;

import java.math.BigDecimal;

public class ThreeForTwoOffer implements Offer {

    private static final BigDecimal ZERO = new BigDecimal("0.00");
    private int count = 0;
    private final Product product;

    public ThreeForTwoOffer(Product product) {
        this.product = product;
    }

    @Override
    public BigDecimal applyTo(Product product) {
        if(!this.product.equals(product)) {
            return ZERO;
        }

        count++;
        if(count > 2) {
            count = 0;
        }
        return count == 0 ? ZERO : product.getPrice();

    }
}
