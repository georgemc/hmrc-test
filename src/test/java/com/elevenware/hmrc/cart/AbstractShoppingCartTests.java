package com.elevenware.hmrc.cart;

import com.elevenware.hmrc.cart.offers.NoOffers;
import com.elevenware.hmrc.cart.offers.ThreeForTwoOffer;
import com.elevenware.hmrc.cart.offers.TwoForOneOffer;
import org.junit.Before;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public abstract class AbstractShoppingCartTests {


    protected static final Product APPLE = new Product("Apple", new BigDecimal("0.60"));
    protected static final Product ORANGE = new Product("Orange", new BigDecimal("0.25"));
    protected ShoppingCart cart;
    protected Till till;

    void assertReceipt(String amount) {

        Receipt receipt = till.checkout(cart);
        assertEquals(new BigDecimal(amount), receipt.getTotal());
        
    }

    @Before
    public void setup() {

        cart = new InMemoryShoppingCart();
        till = new SimpleTill();
        addOffersToTill(till);

    }
    
    protected abstract void addOffersToTill(Till till);

}
