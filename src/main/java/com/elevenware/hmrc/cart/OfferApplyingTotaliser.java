package com.elevenware.hmrc.cart;

import com.elevenware.hmrc.cart.offers.Offer;

import java.util.List;

public class OfferApplyingTotaliser implements ShoppingCartVisitor {
    
    private final Offer offer;
    private final Receipt receipt;

    public OfferApplyingTotaliser(Offer offer, Receipt receipt) {
        this.offer = offer;
        this.receipt = receipt;
    }

    @Override
    public void visit(Product product) {
       receipt.add(offer.applyTo(product));
    }
}
