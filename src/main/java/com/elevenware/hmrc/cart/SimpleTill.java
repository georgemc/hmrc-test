package com.elevenware.hmrc.cart;

import com.elevenware.hmrc.cart.offers.Offer;

import java.util.ArrayList;
import java.util.List;

public class SimpleTill implements Till {
    private List<Offer> offers = new ArrayList<>();

    @Override
    public Receipt checkout(ShoppingCart cart) {
        Receipt receipt = new Receipt();
        for(Offer offer: offers) {
            ShoppingCartVisitor totaliser = new OfferApplyingTotaliser(offer, receipt);
            cart.visit(totaliser);
        }
        return receipt;
    }

    @Override
    public void addOffer(Offer offer) {
        this.offers.add(offer);
    }
}
