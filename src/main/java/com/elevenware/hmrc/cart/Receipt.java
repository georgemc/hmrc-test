package com.elevenware.hmrc.cart;

import java.math.BigDecimal;

public class Receipt {
    
    private BigDecimal total = new BigDecimal("0.00");

    public BigDecimal getTotal() {
        return total;
    }

    public void add(BigDecimal price) {
        total = total.add(price);
    }
}
