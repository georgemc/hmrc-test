package com.elevenware.hmrc.cart;

import java.util.ArrayList;
import java.util.List;

public class InMemoryShoppingCart implements ShoppingCart {
    
    private List<Product> products = new ArrayList<>();
    
    @Override
    public void scan(Product product) {
          products.add(product);
    }

    @Override
    public void visit(ShoppingCartVisitor visitor) {
        products.forEach(visitor::visit);
    }
}
