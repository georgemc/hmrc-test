package com.elevenware.hmrc.cart;

import com.elevenware.hmrc.cart.offers.NoOffers;
import org.junit.Test;

public class ShoppingCartTests extends AbstractShoppingCartTests {

    @Test
    public void cartAddsSingleProductOnce() {
        
        cart.scan(APPLE);

        assertReceipt("0.60");

    }

    @Test
    public void cartAddsSingleProductSeveralTimes() {

        cart.scan(APPLE);
        cart.scan(APPLE);
        cart.scan(APPLE);

        assertReceipt("1.80");

    }

    @Test
    public void cartAddsMultipleProductsSeveralTimes() {

        cart.scan(APPLE);
        cart.scan(APPLE);
        cart.scan(ORANGE);
        cart.scan(ORANGE);
        cart.scan(APPLE);
        cart.scan(ORANGE);

        assertReceipt("2.55");

    }

    @Test
    public void cartBehavesAsExampleInExercise() {

        cart.scan(APPLE);
        cart.scan(APPLE);
        cart.scan(ORANGE);
        cart.scan(APPLE);

        assertReceipt("2.05");

    }

    @Override
    protected void addOffersToTill(Till till) {
        till.addOffer(new NoOffers());
    }
}
